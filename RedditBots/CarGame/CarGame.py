import praw
import datetime
import gspread
import logging
import sys
import json
import time
	
def getAssumedDate():
  
	# Current date
	Now = datetime.datetime.today()
	
	# Push over if needed
	#if Now.weekday() == 1: # Tue. to Wed.
	#	Now += timedelta(days=1)
	#elif Now.weekday() == 3: # Thu. to Fri.
	#	Now += timedelta(days=1)
	#elif Now.weekday() == 6: # Sun. to Mon.
	#	Now += timedelta(days=1)
	
	# Done!
	return Now.strftime("%m/%d/%y")
	
def loadSettings():
    """Load settings from file."""
    try:
        settingsFile = open("settings.json", "r")
    except IOError:
        logging.exception("Error opening settings.json.")
        exitApp()
    
    settingStr = settingsFile.read()
    settingsFile.close()

    try:
        settings = json.loads(settingStr)
    except ValueError:
        logging.exception("Error parsing settings.json.")
        exitApp()
    
    # Check integrity
    if (len(settings["reddit_username"]) == 0):
        logging.critical("Reddit username not set.")
        exitApp()

    if (len(settings["reddit_password"]) == 0):
        logging.critical("Reddit password not set.")
        exitApp()

    if (len(settings["reddit_subreddit"]) == 0):
        logging.critical("Subreddit not set.")
        exitApp()

    if (len(settings["reddit_ua"]) == 0):
        logging.critical("Reddit bot user agent not set.")
        exitApp()

    return settings

def exitApp():
    sys.exit(1)
	
def getNextLink():
	sht = gc.open_by_url('https://docs.google.com/spreadsheets/d/12bExLSzFvajtOzSI06GdXXaEQhvxkLoLJVyuAJ1Qaqg/edit?usp=sharing')
	worksheet = sht.get_worksheet(0)
	val = worksheet.acell('C' + str(settings["post_count"])).value
	return val
	
def getLastCar():
	sht = gc.open_by_url('https://docs.google.com/spreadsheets/d/12bExLSzFvajtOzSI06GdXXaEQhvxkLoLJVyuAJ1Qaqg/edit?usp=sharing')
	worksheet = sht.get_worksheet(0)
	val = worksheet.acell('B' + str((settings["post_count"] - 1))).value
	return val
	
def getNextCar():
	global carName
	sht = gc.open_by_url('https://docs.google.com/spreadsheets/d/12bExLSzFvajtOzSI06GdXXaEQhvxkLoLJVyuAJ1Qaqg/edit?usp=sharing')
	worksheet = sht.get_worksheet(0)
	carName = worksheet.acell('B' + str((settings["post_count"]))).value

def getUsername():
	sht = gc.open_by_url('https://docs.google.com/spreadsheets/d/12bExLSzFvajtOzSI06GdXXaEQhvxkLoLJVyuAJ1Qaqg/edit?usp=sharing')
	worksheet = sht.get_worksheet(0)
	val = worksheet.acell('D' + str(settings["post_count"])).value
	return val

def makePost():
	with open("settings.json", "r") as jsonFile:
		data = json.load(jsonFile)

		tmp = data["post_count"]
		data["post_count"] = tmp + 1
		
	with open("settings.json", "w") as jsonFile:
		jsonFile.write(json.dumps(data))
		
	r.get_subreddit('CarGame').submit( getAssumedDate(), getNextLink() + ' Todays image is brought to you by ' + getUsername() + ' Yesterdays car was a ' + getLastCar())	

def checkAnswers(): 
	submissions = r.get_subreddit('cargame').get_new(limit=1)
	
	for x in submissions:
		print(x)
		comments = praw.helpers.flatten_tree(x.comments)
		
	for comment in comments:
		if carName in str(comment) and comment.id not in already_done:
			comment.reply("Correct! Removed so you don't spoil it for others.")
			comment.remove(spam=False)
			already_done.add(comment.id)

def main():
	global currentTime
	if currentTime == getAssumedDate():
		checkAnswers()
		print('checking')
	else:
		currentTime = getAssumedDate()
		makePost()
		print('posting')
	

global already_done	
settings = loadSettings()
r = praw.Reddit(user_agent=settings["reddit_ua"])
r.login(settings["reddit_username"], settings["reddit_password"])
	
gc = gspread.login(settings["google_username"], settings["google_password"])

currentTime = getAssumedDate()
getNextCar()
already_done = set()

while(True):
	main()
	print("sleeping")
	time.sleep(600)
	
	
	



