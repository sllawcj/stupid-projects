import java.awt.*;
import java.io.IOException;
import java.net.*;
import java.util.Random;
import java.util.Scanner;
import javax.swing.JOptionPane;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

/**
 * Created with IntelliJ IDEA.
 * User: CJ-2
 * Date: 11/18/13
 * Time: 9:43 PM
 * To change this template use File | Settings | File Templates.
 */
public class Main
{
    static String steamUserName = null;
    static String steamID;
    static String game;
    static String gameName;
    static int i;

    public static void main(String [] args)
    {
        steamUserName = JOptionPane.showInputDialog("Please enter your STEAM USERNAME, not your alias!") ;
        if(steamUserName == null)
        {
            System.exit(75);
        }

        getSteamID();
        display();
    }

    private static void display()
    {

        game = getGameID(steamID);
        gameName = getGameName(steamID);


        int n = JOptionPane.showConfirmDialog(
                null,
                "Do you want to play " + gameName + " ?",
                "Game Confirmation",
                JOptionPane.YES_NO_CANCEL_OPTION);

        if(n == JOptionPane.YES_OPTION)
        {
            try
            {
                Desktop.getDesktop().browse(new URI("steam://run/" + game));
            }
            catch (IOException e)
            {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            catch (URISyntaxException e)
            {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        else if(n == JOptionPane.CANCEL_OPTION)
        {
            System.exit(5);
        }
        else
        {
            display();
        }
    }

    private static String getGameName(String steamID)
    {
        Document doc = getXML( "http://api.steampowered.com/IPlayerService/GetOwnedGames/v0001/?key=F9EB640B5EE11DF99DC8604045BFE69C&steamid="+ steamID + "&include_appinfo=1&format=xml");
        NodeList name = doc.getElementsByTagName("name");

        return name.item(i).getTextContent();
    }

    private static String getGameID(String steamID)
    {
        Document doc = getXML( "http://api.steampowered.com/IPlayerService/GetOwnedGames/v0001/?key=F9EB640B5EE11DF99DC8604045BFE69C&steamid="+ steamID + "&include_appinfo=1&format=xml");
        NodeList appid = doc.getElementsByTagName("appid");
        Random rand = new Random();
        i = rand.nextInt(appid.getLength());

        return appid.item(i).getTextContent();
    }

    private static void getSteamID()
    {
        Document doc = getXML("http://steamcommunity.com/id/" + steamUserName + "/?xml=1");
        NodeList list = doc.getElementsByTagName("steamID64");
        steamID = list.item(0).getTextContent();
    }

    private static Document getXML(String u)
    {
        URL url = null;
        try
        {
            url = new URL(u);
        } catch (MalformedURLException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        URLConnection connection = null;
        try
        {
            connection = url.openConnection();
        } catch (IOException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        DocumentBuilderFactory objDocumentBuilderFactory = null;
        DocumentBuilder objDocumentBuilder = null;
        Document doc = null;
        try
        {
            objDocumentBuilderFactory = DocumentBuilderFactory.newInstance();
            objDocumentBuilder = objDocumentBuilderFactory.newDocumentBuilder();

            doc = objDocumentBuilder.parse(connection.getInputStream());
        }
        catch(Exception ex)
        {
            try
            {
                throw ex;
            } catch (Exception e)
            {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }

        return doc;
    }
}